package cz.fel.omo.pavelpa2.smarthome.factories.resourceFactory;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public interface ResourceFactory{
    public Resource makeResource(int onStateHourlyConsumption);
}
