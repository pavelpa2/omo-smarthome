package cz.fel.omo.pavelpa2.smarthome.model.householdmember.fixappliancestrategy;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;

public class FixApplianceStrategyFixYourself implements FixApplianceStrategy{
    @Override
    public boolean fixAppliance(Appliance appliance) {
        appliance.setApplianceState(appliance.getStatePool().getOffState());
        return true;
    }
}
