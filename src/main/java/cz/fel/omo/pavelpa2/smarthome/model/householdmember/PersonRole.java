package cz.fel.omo.pavelpa2.smarthome.model.householdmember;

import java.util.Random;

public enum PersonRole {
    FATHER, MOTHER, CHILD;

    public static PersonRole getRandomEduc(){
        return PersonRole.values()[new Random().nextInt(PersonRole.values().length)];
    }
}
