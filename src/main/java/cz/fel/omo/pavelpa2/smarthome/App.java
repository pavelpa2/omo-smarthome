package cz.fel.omo.pavelpa2.smarthome;

import cz.fel.omo.pavelpa2.smarthome.factories.BasicHouseFactory;
import cz.fel.omo.pavelpa2.smarthome.factories.HouseFactory;
import cz.fel.omo.pavelpa2.smarthome.factories.PoorHouseFactory;
import cz.fel.omo.pavelpa2.smarthome.model.House;
import cz.fel.omo.pavelpa2.smarthome.model.Reporter;

import java.io.IOException;

public class App {
    public static void main(String[] args) {
        try (Reporter reporter = Reporter.getInstance()) {
            HouseFactory houseFactory = new BasicHouseFactory();
            House house = houseFactory.buildHouse();
            Simulation smartHomeSimulation = new SmartHomeSimulation(house, reporter);
            smartHomeSimulation.simulate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
