package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.ArrayList;
import java.util.List;

public class AirConditioner extends Appliance implements HeatingObservable{
    private int targetRoomTemperature;
    private final List<Heating> heatingListeners = new ArrayList<>();

    public AirConditioner() {

    }
    public AirConditioner(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    public void setTargetRoomTemperature(int targetRoomTemperature){
        this.targetRoomTemperature = targetRoomTemperature;
    }

    public void registerHeating (List<Heating> h){
        for (Heating e : h){
            this.registerHeating(e);
        }
    }

    public void registerHeating(Heating h){
       if (this.heatingListeners.contains(h)) {
           return;
       }
       this.heatingListeners.add(h);
    }

    private void notifyHeatingObservers(){
        this.getReporter().writeEvent("AirConditioner#notifyHeatingObservers Turning off heating observers", 2);
        for (Heating h : this.heatingListeners){
            h.getNotified("OFF");
        }
    }

    public void manuallySetTargetRoomTemperature(int targetRoomTemperature){
        int currentRoomTemp = readRoomTemperature();
        if (targetRoomTemperature < currentRoomTemp){
            this.getReporter().writeEvent("AirConditioner#manuallySetTargetRoomTemperature turning on", 2);
            this.getApplianceState().turnOn(this);
            airConditionRoom(targetRoomTemperature);
            notifyHeatingObservers();
        }
        else {
            this.getReporter().writeEvent("AirConditioner#manuallySetTargetRoomTemperature turning off", 2);
            this.getApplianceState().turnOff(this);
        }
    }

    public void airConditionRoom(int targetRoomTemperature){
        this.getRoom().setTemperature(targetRoomTemperature);
    }

    @JsonIgnore
    public int readRoomTemperature(){
        return this.getRoom().getTemperature();
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return h.interact(this);
    }
}
