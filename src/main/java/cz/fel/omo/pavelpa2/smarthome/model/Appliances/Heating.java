package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.List;

public class Heating extends Appliance implements HeatingObserver{
    public Heating() {

    }

    public Heating(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    public void getNotified(String code){
        this.getReporter().writeEvent("Heating#getNotified turning "+code, 3);
        if (code.equals("OFF")){
            this.getApplianceState().turnOff(this);
        } else if (code.equals("ON")){
            this.getApplianceState().turnOn(this);
        }
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return h.interact(this);
    }
}
