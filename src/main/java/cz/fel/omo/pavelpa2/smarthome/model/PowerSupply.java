package cz.fel.omo.pavelpa2.smarthome.model;

public class PowerSupply extends Supply {
    private static PowerSupply instance;

    private PowerSupply() {
        this.setPriceForUnit(0.5);
    }

    public static PowerSupply getInstance() {
        if (instance == null) {
            instance = new PowerSupply();
            instance.setType(SupplyType.PowerSupply);
        }
        return instance;
    }
}
