package cz.fel.omo.pavelpa2.smarthome.factories;

import cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory.*;
import cz.fel.omo.pavelpa2.smarthome.model.*;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class HouseFactory {
    Person father = (Person)Generator.generateFather();
    Person mother = Generator.generateMother();
    Person godfather = Generator.generateGodfather();
    Dog dog = Generator.generateDog();
    Cat cat = Generator.generateCat();
    Child child = (Child)Generator.generateChild();
    House house = new House();

    protected class ApplianceFactoryPool {
        ApplianceFactory airConditionerFactory = new AirConditionerFactory();
        ApplianceFactory dishwasherFactory = new DishwasherFactory();
        ApplianceFactory dryerFactory= new DryerFactory();
        ApplianceFactory heatingFactory= new HeatingFactory();
        ApplianceFactory humiditySensorFactory= new HumiditySensorFactory();
        ApplianceFactory refrigiratorFactory= new RefrigiratorFactory();
        ApplianceFactory thermostatFactory= new ThermostatFactory();
        ApplianceFactory washingMachineFactory= new WashingMachineFactory();
        ApplianceFactory weatherStationFactory= new WeatherStationFactory();
    }

    ApplianceFactoryPool applianceFactoryPool = new ApplianceFactoryPool();

    private HouseFactory addSuppliesToHouse(){
        Supply waterSupply = WaterSupply.getInstance();
        Supply electricitySupply = PowerSupply.getInstance();
        List<Supply> supplies = new ArrayList<>();
        supplies.add(waterSupply);
        supplies.add(electricitySupply);
        house.setSupplies(supplies);
        return this;
    }
    public House buildHouse() throws IOException {
        addSuppliesToHouse()
                .buildFirstFloor()
                .buildSecondFloor()
                .addHouseholdMembers();

        return house;
    }

    protected abstract HouseFactory buildFirstFloor();
    protected abstract HouseFactory buildSecondFloor();


    protected void addSportsEquipmentToRoom(Room room){
        SportsEquipment sportsEquipment = Generator.generateSportsEquipment();
        room.addSportsEquipment(sportsEquipment);
    }

    protected HouseFactory addHouseholdMembers(){
        List<HouseholdMember> householdMembers = new ArrayList<>();
        householdMembers.add(father);
        householdMembers.add(mother);
        householdMembers.add(godfather);
        householdMembers.add(dog);
        householdMembers.add(cat);
        householdMembers.add(child);


        child.setNextChildCarerNode(father);
        father.setNextChildCarerNode(mother);
        mother.setNextChildCarerNode(godfather);
        house.addHouseholdMember(householdMembers);
        return this;
    }
}
