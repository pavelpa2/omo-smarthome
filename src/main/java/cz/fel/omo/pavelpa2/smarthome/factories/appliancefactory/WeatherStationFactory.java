package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.WeatherStation;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public class WeatherStationFactory extends ApplianceFactory{
    @Override
    public Appliance makeAppliance() {
        WeatherStation dryer = new WeatherStation();

        Resource electricity = this.elResourceFactory.makeResource(100);

        dryer.setApplianceType(ApplianceType.WEATHER_STATION);
        dryer.addResource(electricity);

        dryer.setApplianceState(dryer.getStatePool().getOnState());

        return dryer;
    }
}
