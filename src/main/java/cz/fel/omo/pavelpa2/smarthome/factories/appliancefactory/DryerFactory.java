package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Dryer;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public class DryerFactory extends ApplianceFactory{
    @Override
    public Appliance makeAppliance() {
        Dryer dryer = new Dryer();
        dryer.setDocumentation("Documentation");


        Resource water = this.waterResourceFactory.makeResource(100);
        Resource electricity = this.elResourceFactory.makeResource(100);
        dryer.setApplianceType(ApplianceType.DRYER);

        dryer.addResource(water);
        dryer.addResource(electricity);

        dryer.setApplianceState(dryer.getStatePool().getOffState());

        return dryer;
    }
}
