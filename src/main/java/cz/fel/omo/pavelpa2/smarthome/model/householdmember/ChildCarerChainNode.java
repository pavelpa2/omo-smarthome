package cz.fel.omo.pavelpa2.smarthome.model.householdmember;

public interface ChildCarerChainNode {
    void handleChildCrying(Child child);
    void setNextChildCarerNode(Person p);
    void deleteNextChildCarerNode();
    Person getNextChildCarerNode();
}
