package cz.fel.omo.pavelpa2.smarthome.model;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;

import java.util.List;
import java.util.Optional;

public class Floor {
    private int level;
    private List<Room> rooms;

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void setRooms(List<Room> rooms) {
        this.rooms = rooms;
    }

    protected Optional<Room> findRoomWithAFridge(){
        return this.rooms.stream().filter(r -> r.getAppliances().stream().anyMatch(a -> a.getApplianceType() == ApplianceType.REFRIGIRATOR)).findAny();
    }
}
