package cz.fel.omo.pavelpa2.smarthome.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;

import java.io.*;
import java.net.URL;
import java.nio.file.Path;

public class Reporter implements AutoCloseable{
    private static Reporter instance;
    private final ObjectMapper objectMapper;
    private final Path inputHouseConfigPath = Path.of("/houseConfigReports/houseInit.json");
    private final Path outputHouseConfigPath= Path.of("/houseConfigReports/houseEnd.json");
    private final long currentTimeMilis = System.currentTimeMillis();
    private final Path eventOutputPath = Path.of("/eventReports/event" + currentTimeMilis + "txt");
//    private final URL eventOutputURL = getClass().getResource("/eventReports/event.txt");
    private final Path financialReportPath = Path.of("/financialReports/financialReport" + currentTimeMilis + ".txt");
    private final String projectPath = System.getProperty("user.dir");
    private Writer eventWriter;
    private Writer financialWriter;

    private Reporter() throws IOException {
         objectMapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);
         try{
             eventWriter = new BufferedWriter(new FileWriter(projectPath+eventOutputPath, true));
             financialWriter = new BufferedWriter(new FileWriter(projectPath+financialReportPath, true));;
         }catch (IOException e) {
             e.printStackTrace();
         }
    }

    public void writeInputHouseConfig(House house) throws IOException {
        objectMapper.writeValue(new File(projectPath+ this.inputHouseConfigPath), house);
    }

    public void writeOutputHouseConfig(House house) throws IOException {
        objectMapper.writeValue(new File(projectPath+ this.outputHouseConfigPath), house);
    }

    public void writeEvent(String event, int level){
        try{
            prepareEventMargin(level);
            eventWriter.append(event);
            eventWriter.append("\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prepareEventMargin(int level){
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < level; i++){
            stringBuilder.append("   ");
        }
        try{
            eventWriter.append(stringBuilder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeFinancialReport(House house){
        try {
            for (Appliance a : house.getAllAppliances()){
                financialWriter.append(a.getClass().getSimpleName() + "\n");
                for (Resource r : a.getResources()){
                    financialWriter.append("   "+r.getResourceType().toString()+ " total consumed: " + r.getConsumedTotal() + " total price: " + r.calculateTotalPrice());
                    financialWriter.append("\n");
                }
            }
            financialWriter.append("-------------------------TOTAL------------------\n");
            for (Supply s : house.getSupplies()){
                financialWriter.append(s.getType().toString() + " total supplied: " + s.getTotalSupplied()+ " total price: " + s.calculateTotalPrice());
                financialWriter.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void closeWriter() throws IOException {

    }

    public static Reporter getInstance(){
        if (instance == null) {
            try {
                instance = new Reporter();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    @Override
    public void close() throws Exception {
        try{
            eventWriter.close();
            financialWriter.close();
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
