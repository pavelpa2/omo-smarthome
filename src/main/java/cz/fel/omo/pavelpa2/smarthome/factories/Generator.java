package cz.fel.omo.pavelpa2.smarthome.factories;

import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.*;
import cz.fel.omo.pavelpa2.smarthome.util.Utilities;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Generator {
    Random random = new Random();

    private static double getRandomDouble(){
        return Utilities.getRandomNumber(1000, 100000);
    }

    private static double getRandomInt(){
        return Utilities.getRandomNumber(1000, 100000);
    }

    public static Person generatePerson(){
        Person person = new Person(Education.getRandomEduc(),getRandomDouble(), PersonRole.getRandomEduc());
        person.setSportsEquipment(null);
        person.setName("Person" + getRandomInt());
        person.setHunger(Utilities.getRandomNumber(0, 100));
        return person;
    }

    public static Person generateFather(){
        Person p = generatePerson();
        p.setName("Father" + getRandomInt());
        return p;
    }

    public static Person generateMother(){
        Person p = generatePerson();
        p.setName("Mother" + getRandomInt());
        return p;
    }
    public static Person generateGodfather(){
        Person p = generatePerson();
        p.setName("Godfather" + getRandomInt());
        return p;
    }


    public static Child generateChild(){
        Child person = new Child(null, getRandomDouble(), PersonRole.CHILD);
        person.setSportsEquipment(null);
        person.setName("Child" + getRandomInt());
        person.setHunger(Utilities.getRandomNumber(0, 100));
        return person;
    }

    public static Dog generateDog(){
        Dog dog = new Dog();
        dog.setName("Dog" + getRandomInt());
        dog.setHunger(Utilities.getRandomNumber(0, 100));
        return dog;
    }

    public static Cat generateCat(){
        Cat dog = new Cat();
        dog.setName("Cat" + getRandomInt());
        dog.setHunger(Utilities.getRandomNumber(0, 100));
        return dog;
    }

    public static Room generateRoom(){
        Room room = new Room();
        room.setTemperature(Utilities.getRandomNumber(15, 30));
        List<SportsEquipment> sportsEquipmentList = new ArrayList<>();
        if (new Random().nextBoolean()){
            sportsEquipmentList.add(generateSportsEquipment());
        }
        room.setSportsEquipmentList(sportsEquipmentList);
        return room;
    }

    public static SportsEquipment generateSportsEquipment(){

        SportsEquipment sportsEquipment = new SportsEquipment();
        sportsEquipment.setName("SportsEquipment"+getRandomInt());
        return sportsEquipment;
    }
}
