package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.AirConditioner;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public class AirConditionerFactory extends ApplianceFactory{
    @Override
    public Appliance makeAppliance() {
        AirConditioner airConditioner = new AirConditioner();
        airConditioner.setTargetRoomTemperature(20);


        Resource water = this.waterResourceFactory.makeResource(100);
        Resource electricity = this.elResourceFactory.makeResource(100);

        airConditioner.setApplianceType(ApplianceType.AIR_CONDITIONER);

        airConditioner.addResource(water);
        airConditioner.addResource(electricity);

        airConditioner.setApplianceState(airConditioner.getStatePool().getOffState());

        return airConditioner;
    }
}
