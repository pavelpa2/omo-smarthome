package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.ArrayList;
import java.util.List;

public class Thermostat extends Appliance implements HeatingObservable{
    private int targetRoomTemperature;
    private final List<Heating> heatingListeners = new ArrayList<>();

    public Thermostat() {

    }

    public Thermostat(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return h.interact(this);
    }

    public int readRoomTemperature(Room room){
        return room.getTemperature();
    }

    public void registerHeating (List<Heating> h){
        for (Heating e : h){
            this.registerHeating(e);
        }
    }

    public void registerHeating(Heating h){
        if (this.heatingListeners.contains(h)) {
            return;
        }
        this.heatingListeners.add(h);
    }

    public void turnOnHeating(){
        this.notifyHeatingObservers("ON");
    }

    public int readRoomTemperature(){
        return this.getRoom().getTemperature();
    }

    public void turnOffHeating(){
        this.notifyHeatingObservers("OFF");
    }

    private void notifyHeatingObservers(String code){
        for (Heating h : this.heatingListeners){
            h.getNotified(code);
        }
    }

    private void heatRoom(int targetRoomTemperature){
        this.getRoom().setTemperature(targetRoomTemperature);
    }

    public void setTargetRoomTemperature(int targetRoomTemperature){
        int currentRoomTemp = readRoomTemperature();
        if (targetRoomTemperature < currentRoomTemp){
            this.getReporter().writeEvent("Thermostat#setTargetRoomTemperature target temp was set to lower than current temp of room, turning off heating", 2);
            turnOffHeating();
        }
        else {
            this.getReporter().writeEvent("Thermostat#setTargetRoomTemperature target temp was set to higher than current temp of room, turning on heating", 2);
            heatRoom(targetRoomTemperature);
            turnOnHeating();
        }
    }
}
