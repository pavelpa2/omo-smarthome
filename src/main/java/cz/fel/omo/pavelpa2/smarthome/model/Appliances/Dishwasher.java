package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;
import cz.fel.omo.pavelpa2.smarthome.util.Utilities;

import java.util.List;

public class Dishwasher extends Appliance implements SensorObservable{
    private int dishesCapacity;
    private Sensor humiditySensorNode;

    public Dishwasher() {

    }

    public Dishwasher(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    public int getDishesCapacity() {
        return dishesCapacity;
    }

    public void setDishesCapacity(int dishesCapacity) {
        this.dishesCapacity = Math.min(dishesCapacity, 100);
    }

    public void registerSensor(Sensor humiditySensor){
        this.humiditySensorNode = humiditySensor;
    }

    public void notifySensor(){
       this.humiditySensorNode.beep();
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return h.interact(this);
    }

    public void insertDish(){
       this.setDishesCapacity(this.getDishesCapacity() + Utilities.getRandomNumber(7, 25));
    }

    public void unloadDishwasher(){
        this.setDishesCapacity(0);
    }
}
