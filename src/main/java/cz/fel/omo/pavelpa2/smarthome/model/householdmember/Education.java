package cz.fel.omo.pavelpa2.smarthome.model.householdmember;

import java.util.Random;

public enum Education {
    HIGH_SCHOOL, UNI;

    public static Education getRandomEduc(){
        return Education.values()[new Random().nextInt(Education.values().length)];
    }
}
