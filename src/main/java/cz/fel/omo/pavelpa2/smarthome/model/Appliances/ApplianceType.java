package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

public enum ApplianceType {
    AIR_CONDITIONER, DISHWASHER, DRYER, HEATING, HUMIDITY_SENSOR, REFRIGIRATOR, THERMOSTAT, WASHING_MACHINE, WEATHER_STATION
}

