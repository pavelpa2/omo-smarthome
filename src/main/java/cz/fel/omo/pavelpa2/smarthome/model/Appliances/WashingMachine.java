package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.List;

public class WashingMachine extends Appliance implements SensorObservable{
    private Sensor humiditySensorNode;

    public WashingMachine() {
    }

    public WashingMachine(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    public void registerSensor(Sensor humiditySensor){
        this.humiditySensorNode = humiditySensor;
    }

    public void notifySensor(){
        this.humiditySensorNode.beep();
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return h.interact(this);
    }
}
