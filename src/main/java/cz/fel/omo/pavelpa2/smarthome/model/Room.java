package cz.fel.omo.pavelpa2.smarthome.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Refrigirator;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.SportsEquipment;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Room {
    @JsonManagedReference
    private List<Appliance> appliances = new ArrayList<>();
    private List<SportsEquipment> sportsEquipmentList = new ArrayList<>();
    private int temperature;
    @JsonManagedReference
    private List<HouseholdMember> householdMembers = new ArrayList<>();

    public void setAppliances(List<Appliance> appliances) {
        this.appliances = appliances;
    }

    public List<SportsEquipment> getSportsEquipmentList() {
        return sportsEquipmentList;
    }

    public void setSportsEquipmentList(List<SportsEquipment> sportsEquipmentList) {
        this.sportsEquipmentList = sportsEquipmentList;
    }

    public int getTemperature() {
        return temperature;
    }

    public void setTemperature(int temperature) {
        this.temperature = temperature;
    }

    public List<HouseholdMember> getHouseholdMembers() {
        return new ArrayList<>(householdMembers);
    }

    public List<Appliance> getAppliances() {
        return new ArrayList<>(this.appliances);
    }

    public void addAppliance(Appliance appliance){
        if (this.appliances.contains(appliance)){
            return;
        }
        this.appliances.add(appliance);
        appliance.setRoom(this);
    }

    @JsonIgnore
    public Refrigirator getRefrigiratorInRoom(){
        Optional<Appliance> appliance = this.getAppliances().stream().filter(a -> a.getApplianceType() == ApplianceType.REFRIGIRATOR).findFirst();
        if (appliance.isEmpty()){
            return null;
        }
        return (Refrigirator) appliance.get();
    }

    public void setHouseholdMembers(List<HouseholdMember> householdMembers) {
        for (HouseholdMember h : householdMembers){
            h.setRoom(this);
        }
        this.householdMembers = householdMembers;
    }

    public void addHouseholdMember(HouseholdMember householdMember){
        if (this.householdMembers.contains(householdMember)){
            return;
        }
        this.householdMembers.add(householdMember);
        householdMember.setRoom(this);
    }

    public void addSportsEquipment(SportsEquipment sportsEquipment){
        if (!this.sportsEquipmentList.contains(sportsEquipment)){
            this.sportsEquipmentList.add(sportsEquipment);
        }
    }

    @JsonIgnore
    public SportsEquipment getAvailableSportsEquipment(){
        if (this.sportsEquipmentList.isEmpty()){
            return null;
        }
        SportsEquipment toReturn = sportsEquipmentList.get(0);
        sportsEquipmentList.remove(0);
        return toReturn;
    }
}
