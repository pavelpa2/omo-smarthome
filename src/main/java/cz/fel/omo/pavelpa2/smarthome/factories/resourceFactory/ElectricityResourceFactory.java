package cz.fel.omo.pavelpa2.smarthome.factories.resourceFactory;

import cz.fel.omo.pavelpa2.smarthome.model.PowerSupply;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.ResourceType;

public class ElectricityResourceFactory implements ResourceFactory{

    @Override
    public Resource makeResource(int onStateHourlyConsumption) {
        Resource elResource = new Resource();
        elResource.setResourceType(ResourceType.ELECTRICITY);
        elResource.setSupply(PowerSupply.getInstance());
        elResource.setOnStateHourlyConsumption(onStateHourlyConsumption);
        elResource.setConsumedTotal(0);
        return elResource;
    }
}
