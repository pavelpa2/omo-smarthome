package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.List;

public class HumiditySensor extends Sensor implements SensorObserver, Alarm{
    public HumiditySensor() {
        super();
    }

    public HumiditySensor(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    @Override
    public void beep() {
        this.getReporter().writeEvent("HumiditySensor#beep BEEP BEEP BEEP", 2);
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return h.interact(this);
    }

    @Override
    public void getNotified() {
        beep();
    }
}
