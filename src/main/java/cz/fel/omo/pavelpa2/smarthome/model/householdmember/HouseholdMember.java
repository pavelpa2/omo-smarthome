package cz.fel.omo.pavelpa2.smarthome.model.householdmember;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.*;
import cz.fel.omo.pavelpa2.smarthome.model.House;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.ApplianceVisitor.ConcreteApplianceInteract;

import java.util.Collections;
import java.util.List;

import cz.fel.omo.pavelpa2.smarthome.model.Reporter;
import java.util.Random;
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "hmType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Person.class, name = "person"),
        @JsonSubTypes.Type(value = Pet.class, name = "pet"),
        @JsonSubTypes.Type(value = Child.class, name = "child"),
        @JsonSubTypes.Type(value = Dog.class, name = "dog"),
        @JsonSubTypes.Type(value = Cat.class, name = "cat"),
})
public class HouseholdMember implements ConcreteApplianceInteract{
    @JsonIgnore
    private Reporter reporter = Reporter.getInstance();
    private String name;
    private int hunger;
    @JsonBackReference
    private Room room;
    @JsonBackReference
    private House house;

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHunger() {
        return hunger;
    }

    public void setHunger(int hunger) {
        this.hunger = hunger;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    private List<Appliance> getAppliancesInCurrentRoom(){
        return this.room.getAppliances();
    }

    protected void leaveRoom(){
        List<HouseholdMember> listOfPeopleInMyRoom = room.getHouseholdMembers();
        listOfPeopleInMyRoom.remove(this);
        room.setHouseholdMembers(listOfPeopleInMyRoom);
        this.setRoom(null);
    }

    protected void goOutside(){
       leaveRoom();
    }

    public void comeBackHome(){
        if (this.isOutside()){
            Room roomToComeBackTo = this.house.getRandomRoomInHouse();
            this.setRoom(roomToComeBackTo);
            roomToComeBackTo.addHouseholdMember(this);
        }
    }

    public boolean isOutside(){
        return this.getRoom() == null;
    }

    public void visitRoom(Room room){
        leaveRoom();
        this.setRoom(room);
        room.addHouseholdMember(this);
    }

    public void breakAppliance(Appliance appliance){
        appliance.getApplianceState().breakAppliance(appliance);
    }

    private Appliance getRandomApplianceFromCurrentRoom(){
        List<Appliance> appliancesInCurrentRoom = getAppliancesInCurrentRoom();
        Random r = new Random();
        return appliancesInCurrentRoom.get(r.nextInt(appliancesInCurrentRoom.size()));
    }

    private boolean randomBreakAppliance(Appliance appliance){
        Random random = new Random();
        if (random.nextBoolean()){
            getReporter().writeEvent(this.getClass().getSimpleName()+"#randomBreakAppliance broke " + appliance.getClass().getSimpleName(), 1);
            this.breakAppliance(appliance);
            return true;
        }
        else {
            getReporter().writeEvent(this.getClass().getSimpleName()+"#randomBreakAppliance didnt break " + appliance.getClass().getSimpleName(), 1);
            return false;
        }
    }

    @Override
    public boolean interact(AirConditioner airConditioner) {
        System.out.println(this.getClass().getSimpleName()+"#interact#airconditioner");
        return randomBreakAppliance(airConditioner);
    }

    @Override
    public boolean interact(Dishwasher dishwasher) {
        System.out.println(this.getClass().getSimpleName()+"#interact#dishwasher");
        return randomBreakAppliance(dishwasher);
    }

    @Override
    public boolean interact(Dryer dryer) {
        System.out.println(this.getClass().getSimpleName()+"#interact#dryer");
        return randomBreakAppliance(dryer);
    }

    @Override
    public boolean interact(HumiditySensor fireSensor) {
        System.out.println(this.getClass().getSimpleName()+"#interact#fireSensor");
        return randomBreakAppliance(fireSensor);
    }

    @Override
    public boolean interact(Heating heating) {
        System.out.println(this.getClass().getSimpleName()+"#interact#heating");
        return randomBreakAppliance(heating);
    }

    @Override
    public boolean interact(Refrigirator refrigirator) {
        System.out.println(this.getClass().getSimpleName()+"#interact#refrigirator");
        return randomBreakAppliance(refrigirator);
    }

    @Override
    public boolean interact(WashingMachine washingMachine) {
        System.out.println(this.getClass().getSimpleName()+"#interact#washingMachine");
        return randomBreakAppliance(washingMachine);
    }

    @Override
    public boolean interact(WeatherStation weatherStation) {
        System.out.println(this.getClass().getSimpleName()+"#interact#weatherStation");
        return randomBreakAppliance(weatherStation);
    }

    @Override
    public boolean interact(Thermostat thermostat) {
        System.out.println(this.getClass().getSimpleName()+"#interact#thermostat");
        return randomBreakAppliance(thermostat);
    }

    public void interactWithSomeApplianceInRoom(){
        String event =this.getClass().getSimpleName()+"#interactWithSomeApplianceInRoom " + this.toString();
        getReporter().writeEvent(event, 0);
        System.out.println(event);

        if (this.getRoom() == null){
            return;
        }
        List<Appliance> appliancesInRoom = this.getRoom().getAppliances();
        Collections.shuffle(appliancesInRoom);

        boolean didInteractWithApplianceFlag = false;

        if (appliancesInRoom.isEmpty()){
            getReporter().writeEvent("no appliances in room, moving to random room", 1);
            this.moveToRandomRoom();
        }
        Appliance applianceToInteractWith = appliancesInRoom.get(0);

        didInteractWithApplianceFlag = appliancesInRoom.get(0).householdMemberInteracts(this);
        if (!didInteractWithApplianceFlag){
            getReporter().writeEvent("moving to random room", 1);
            this.moveToRandomRoom();
        }
    }

    public void moveToRandomRoom(){
        Room roomToVisit = this.getHouse().getRandomRoomInHouse();
        roomToVisit.addHouseholdMember(this);
        this.setRoom(roomToVisit);
    }

    public Reporter getReporter() {
        return reporter;
    }

    public void setReporter(Reporter reporter) {
        this.reporter = reporter;
    }
}
