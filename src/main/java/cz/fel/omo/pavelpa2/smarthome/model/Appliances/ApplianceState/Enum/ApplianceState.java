package cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.Enum;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ApplianceState {
    ON("on"), OFF("off"), BROKEN("broken");

    private String name;

    private ApplianceState(String name) {
        this.name = name;
    }
}
