package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.List;

public abstract class Sensor extends Appliance implements Alarm, SensorObserver{
    public Sensor() {

    }

    public Sensor(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return true;
    }
}

