package cz.fel.omo.pavelpa2.smarthome.model.householdmember;

public class SportsEquipment {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
