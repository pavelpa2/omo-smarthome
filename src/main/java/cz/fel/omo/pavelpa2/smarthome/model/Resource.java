package cz.fel.omo.pavelpa2.smarthome.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Resource {
    private ResourceType resourceType;
    private int onStateHourlyConsumption;
    private int consumedTotal;
    @JsonIgnore
    private Supply supply;

    public Supply getSupply() {
        return supply;
    }

    public void setSupply(Supply supply) {
        this.supply = supply;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public int getOnStateHourlyConsumption() {
        return onStateHourlyConsumption;
    }

    public void setOnStateHourlyConsumption(int onStateHourlyConsumption) {
        this.onStateHourlyConsumption = onStateHourlyConsumption;
    }

    public int getConsumedTotal() {
        return consumedTotal;
    }

    public void setConsumedTotal(int consumedTotal) {
        this.consumedTotal = consumedTotal;
    }

    public void addToTotalConsumed(int amount){
       this.setConsumedTotal(this.getConsumedTotal() + amount);
       supply.setTotalSupplied(supply.getTotalSupplied() + amount);
    }

    public double calculateTotalPrice(){
        return this.consumedTotal * supply.getPriceForUnit();
    }
}
