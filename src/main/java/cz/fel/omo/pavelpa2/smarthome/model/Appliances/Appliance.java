package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.*;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.Enum.ApplianceState;
import cz.fel.omo.pavelpa2.smarthome.model.Reporter;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.ApplianceVisitor.Interactable;
import cz.fel.omo.pavelpa2.smarthome.model.Room;

import java.util.ArrayList;
import java.util.List;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AirConditioner.class, name = "AirConditioner"),
        @JsonSubTypes.Type(value = Dishwasher.class, name = "Dishwasher"),
        @JsonSubTypes.Type(value = Dryer.class, name = "Dryer"),
        @JsonSubTypes.Type(value = Heating.class, name = "Heating"),
        @JsonSubTypes.Type(value = HumiditySensor.class, name = "HumiditySensor"),
        @JsonSubTypes.Type(value = Refrigirator.class, name = "Refrigirator"),
        @JsonSubTypes.Type(value = Sensor.class, name = "Sensor"),
        @JsonSubTypes.Type(value = Thermostat.class, name = "Thermostat"),
        @JsonSubTypes.Type(value = WashingMachine.class, name = "WashingMachine"),
        @JsonSubTypes.Type(value = WeatherStation.class, name = "WeatherStation"),
})
public abstract class Appliance implements Interactable, SensorObservable {
    public class StatePool{
        private final cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState onState;
        private final cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState offState;
        private final cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState brokenState;

        public StatePool() {
            this.onState = new OnState();
            this.offState = new OffState();
            this.brokenState = new BrokenState();
        }

        public cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState getOnState() {
            return onState;
        }

        public cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState getOffState() {
            return offState;
        }

        public cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState getBrokenState() {
            return brokenState;
        }
    }

    @JsonIgnore
    private final StatePool statePool = new StatePool();
    @JsonIgnore
    private final Reporter reporter = Reporter.getInstance();
    @JsonIgnore
    private ApplianceType applianceType;
    private String documentation;
    @JsonBackReference
    private Room room;
    private List<Resource> resources = new ArrayList<>();
    private List<Sensor> fatalStateObservers = new ArrayList<>();
    private cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState applianceState =statePool.getOffState();

    public Appliance() {
    }

    public Appliance(double price, String documentation, List<Resource> resources, Room room) {
        this.documentation = documentation;
        this.resources = resources;
        this.applianceState = new OffState();
        this.room = room;
    }

    public Reporter getReporter() {
        return reporter;
    }

    public StatePool getStatePool() {
        return statePool;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getDocumentation() {
        return documentation;
    }

    public void setDocumentation(String documentation) {
        this.documentation = documentation;
    }

    public cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState getApplianceState() {
        return applianceState;
    }

    public void setApplianceState(cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState applianceState) {
        this.applianceState = applianceState;
    }

    public List<Resource> getResources() {
        return resources;
    }

    public ApplianceType getApplianceType() {
        return applianceType;
    }

    public void setApplianceType(ApplianceType applianceType) {
        this.applianceType = applianceType;
    }

    public void setResources(List<Resource> resources) {
        this.resources = resources;
    }

    public boolean isBroken() {
        return this.getApplianceState().getState() == ApplianceState.BROKEN;
    }

    @Override
    public void notifySensor(){
        for (Sensor p : fatalStateObservers){
            p.getNotified();
        }
    }

    @Override
    public void registerSensor(Sensor sensor){
        if (this.fatalStateObservers.contains(sensor)) {
            return;
        }
        this.fatalStateObservers.add(sensor);
    }

    public void addResource(Resource resource){
        if (this.resources.contains(resource)){
            return;
        }
        this.resources.add(resource);
    }
}
