package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.List;

public class Refrigirator extends Appliance{
    private int foodCapacity;

    public Refrigirator() {

    }

    public Refrigirator(double price, String documentation, List<Resource> resources, Room room, int foodCapacity) {
        super(price, documentation, resources, room);
        this.foodCapacity = foodCapacity;
    }

    public int getFoodCapacity() {
        return foodCapacity;
    }

    public void setFoodCapacity(int foodCapacity) {
        if (foodCapacity < 1) {
            this.foodCapacity = 0;
            return;
        }
        this.foodCapacity = foodCapacity;
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
        return h.interact(this);
    }
}
