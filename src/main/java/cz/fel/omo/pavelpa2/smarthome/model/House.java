package cz.fel.omo.pavelpa2.smarthome.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class House {
    @JsonManagedReference
    private List<HouseholdMember> householdMembers = new ArrayList<>();
    private List<Floor> floors = new ArrayList<>();
    private List<Supply> supplies = new ArrayList<>();

    public List<HouseholdMember> getHouseholdMembers() {
        return householdMembers;
    }

    public void setHouseholdMembers(List<HouseholdMember> householdMembers) {
        this.householdMembers = householdMembers;
        for(HouseholdMember h : this.householdMembers){
            h.setHouse(this);
        }
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    public List<Supply> getSupplies() {
        return supplies;
    }

    public void setSupplies(List<Supply> supplies) {
        this.supplies = supplies;
    }

    @JsonIgnore
    public Room getRandomRoomInHouse(){
        Random r = new Random();
        List<Room> rooms = getRandomFloorInHouse().getRooms();
        Room randomRoom = rooms.get(r.nextInt(rooms.size()));
        return randomRoom;
    }

    private Floor getRandomFloorInHouse(){
        Random r = new Random();
        return this.getFloors().get(r.nextInt(this.getFloors().size()));
    }

    public Room findRoomWithAFridge(){
        for (Floor f : this.floors){
            Optional<Room> roomWithAFridge = f.findRoomWithAFridge();
            if (roomWithAFridge.isPresent()){
                return roomWithAFridge.get();
            }
        }
        return null;
    }

    public void addHouseholdMember(List<HouseholdMember> householdMembers){
        for (HouseholdMember h : householdMembers){
            this.addHouseholdMember(h);
        }
    }

    public void addHouseholdMember(HouseholdMember h){
       if (this.householdMembers.contains(h)) {
           return;
       }
       this.householdMembers.add(h);
       h.setHouse(this);
    }

    @JsonIgnore
    public List<Appliance> getAllAppliances(){
        return getAllRooms().stream().flatMap(s->s.getAppliances().stream()).collect(Collectors.toList());
    }

    private List<Room> getAllRooms(){
        return this.getFloors().stream().flatMap(s->s.getRooms().stream()).collect(Collectors.toList());
    }
}
