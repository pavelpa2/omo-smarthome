package cz.fel.omo.pavelpa2.smarthome.model.householdmember.fixappliancestrategy;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;

public class FixApplianceStrategyLazy implements FixApplianceStrategy{
    @Override
    public boolean fixAppliance(Appliance appliance) {
        return false;
    }

}
