package cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState;

import com.fasterxml.jackson.annotation.JsonRootName;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.Enum.ApplianceState;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

import java.util.List;
@JsonRootName(value = "Off")
public class OffState implements cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState {
    public ApplianceState applianceState = ApplianceState.OFF;
    public ApplianceState getApplianceStateEnum(){
        return this.applianceState;
    }
    @Override
    public void turnOn(Appliance context) {
        context.setApplianceState(context.getStatePool().getOnState());
        context.getApplianceState().handleChangeStateConsumption(context);
    }

    @Override
    public void turnOff(Appliance context) {

    }


    @Override
    public void breakAppliance(Appliance context) {
        context.setApplianceState(context.getStatePool().getBrokenState());
        context.getApplianceState().handleChangeStateConsumption(context);
        context.notifySensor();
    }


    @Override
    public void handleConsumption(Appliance context) {
    }

    @Override
    public void handleChangeStateConsumption(Appliance context) {
        consumeEveryResourceByFactor(context, 0);
    }

    @Override
    public void consumeEveryResourceByFactor(Appliance context, int factor) {
        List<Resource> resourceList = context.getResources();
        for (Resource r : resourceList){
            r.addToTotalConsumed(
                    r.getOnStateHourlyConsumption() * factor
            );
        }
    }

    @Override
    public ApplianceState getState() {
        return ApplianceState.OFF;
    }
}
