package cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState;

import com.fasterxml.jackson.annotation.JsonRootName;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.Enum.ApplianceState;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

import java.util.List;

@JsonRootName(value = "On")
public class OnState implements cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.ApplianceState {

    ApplianceState applianceState = ApplianceState.ON;
    @Override
    public void turnOn(Appliance context) {

    }

    @Override
    public void turnOff(Appliance context) {
        context.setApplianceState(context.getStatePool().getOffState());
    }


    @Override
    public void breakAppliance(Appliance context) {
        context.setApplianceState(context.getStatePool().getBrokenState());
        context.getApplianceState().handleChangeStateConsumption(context);
        context.notifySensor();
    }

    @Override
    public void handleConsumption(Appliance context) {
        consumeEveryResourceByFactor(context, 24);
    }

    @Override
    public void handleChangeStateConsumption(Appliance context) {
        consumeEveryResourceByFactor(context, 2);
    }

    @Override
    public void consumeEveryResourceByFactor(Appliance context, int factor) {
        List<Resource> resourceList = context.getResources();
        for (Resource r : resourceList){
            r.addToTotalConsumed(
                    r.getOnStateHourlyConsumption() * factor
            );
        }
    }

    @Override
    public ApplianceState getState() {
        return ApplianceState.ON;
    }
}
