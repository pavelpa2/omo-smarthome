package cz.fel.omo.pavelpa2.smarthome;

public abstract  class Simulation {

    abstract void initialize();
    abstract void startSimulating();
    abstract void endSimulating();

    public final void simulate() {
        initialize();
        startSimulating();
        endSimulating();
    }
}
