package cz.fel.omo.pavelpa2.smarthome.factories;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Heating;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Refrigirator;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Thermostat;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.WeatherStation;
import cz.fel.omo.pavelpa2.smarthome.model.Floor;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.Child;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.util.ArrayList;
import java.util.List;

public class PoorHouseFactory extends HouseFactory{
    Child child2 = Generator.generateChild();
    Child child3 = Generator.generateChild();
    public PoorHouseFactory() {
    }

    @Override
    protected HouseFactory buildFirstFloor() {
        Floor floor1 = new Floor();
        house.getFloors().add(floor1);
        List<Room> rooms1 = new ArrayList<>();
        Room room11 = Generator.generateRoom();
        Room room13 = Generator.generateRoom();
        rooms1.add(room11);
        rooms1.add(room13);
        floor1.setRooms(rooms1);
        floor1.setLevel(0);

        Refrigirator refrigirator = (Refrigirator) applianceFactoryPool.refrigiratorFactory.makeAppliance();
        Thermostat thermostat = (Thermostat)  applianceFactoryPool.thermostatFactory.makeAppliance();

        Heating heating1 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();
        Heating heating2 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();
        Heating heating3 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();


        WeatherStation weatherStation1 = (WeatherStation) applianceFactoryPool.weatherStationFactory.makeAppliance();
        WeatherStation weatherStation2 = (WeatherStation) applianceFactoryPool.weatherStationFactory.makeAppliance();

        thermostat.registerHeating(heating1);
        thermostat.registerHeating(heating2);
        thermostat.registerHeating(heating3);

        room11.addAppliance(refrigirator);
        room11.addAppliance(thermostat);
        room11.addAppliance(heating1);
        room11.addAppliance(heating2);
        room13.addAppliance(heating3);

        room11.addAppliance(weatherStation1);
        room13.addAppliance(weatherStation2);

        this.addSportsEquipmentToRoom(room11);
        this.addSportsEquipmentToRoom(room13);


        room11.addHouseholdMember(father);
        room11.addHouseholdMember(mother);
        room11.addHouseholdMember(child);
        room11.addHouseholdMember(child2);
        room11.addHouseholdMember(child3);
        room13.addHouseholdMember(godfather);
        return this;
    }


    @Override
    protected HouseFactory buildSecondFloor() {
        return this;
    }

    @Override
    protected HouseFactory addHouseholdMembers() {
        List<HouseholdMember> householdMembers = new ArrayList<>();
        householdMembers.add(father);
        householdMembers.add(mother);
        householdMembers.add(godfather);
        householdMembers.add(child);
        householdMembers.add(child2);
        householdMembers.add(child3);


        child.setNextChildCarerNode(father);
        father.setNextChildCarerNode(mother);
        mother.setNextChildCarerNode(godfather);

        child2.setNextChildCarerNode(mother);
        child3.setNextChildCarerNode(mother);

        house.addHouseholdMember(householdMembers);
        return this;
    }
}
