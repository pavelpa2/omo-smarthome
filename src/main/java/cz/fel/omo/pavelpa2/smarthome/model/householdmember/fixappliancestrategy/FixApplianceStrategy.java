package cz.fel.omo.pavelpa2.smarthome.model.householdmember.fixappliancestrategy;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;

public interface FixApplianceStrategy {
    public boolean fixAppliance(Appliance appliance);
}
