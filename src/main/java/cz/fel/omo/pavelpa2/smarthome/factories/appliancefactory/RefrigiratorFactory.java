package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Refrigirator;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public class RefrigiratorFactory extends ApplianceFactory {
    @Override
    public Appliance makeAppliance() {
       Refrigirator dryer = new Refrigirator();

        Resource electricity = this.elResourceFactory.makeResource(100);

        dryer.setApplianceType(ApplianceType.REFRIGIRATOR);
        dryer.addResource(electricity);

        dryer.setApplianceState(dryer.getStatePool().getOnState());

        return dryer;
    }
}
