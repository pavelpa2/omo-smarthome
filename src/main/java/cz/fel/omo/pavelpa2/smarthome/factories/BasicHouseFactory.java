package cz.fel.omo.pavelpa2.smarthome.factories;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.*;
import cz.fel.omo.pavelpa2.smarthome.model.Floor;
import cz.fel.omo.pavelpa2.smarthome.model.Room;

import java.util.ArrayList;
import java.util.List;

public class BasicHouseFactory extends HouseFactory{
    public BasicHouseFactory() {
        super();
    }

    @Override
    protected HouseFactory buildFirstFloor() {
        Floor floor1 = new Floor();
        house.getFloors().add(floor1);
        List<Room> rooms1 = new ArrayList<>();
        Room room11 = Generator.generateRoom();
        Room room12 = Generator.generateRoom();
        Room room13 = Generator.generateRoom();
        rooms1.add(room11);
        rooms1.add(room12);
        rooms1.add(room13);
        floor1.setRooms(rooms1);
        floor1.setLevel(0);

        AirConditioner airConditioner = (AirConditioner) applianceFactoryPool.airConditionerFactory.makeAppliance();
        Dishwasher dishwasher = (Dishwasher) applianceFactoryPool.dishwasherFactory.makeAppliance();
        Dryer dryer = (Dryer) applianceFactoryPool.dryerFactory.makeAppliance();
        WashingMachine washingMachine = (WashingMachine) applianceFactoryPool.washingMachineFactory.makeAppliance();
        HumiditySensor humiditySensor = (HumiditySensor) applianceFactoryPool.humiditySensorFactory.makeAppliance();
        Thermostat thermostat = (Thermostat)  applianceFactoryPool.thermostatFactory.makeAppliance();
        WeatherStation weatherStation1 = (WeatherStation)  applianceFactoryPool.weatherStationFactory.makeAppliance();
        WeatherStation weatherStation2 = (WeatherStation)  applianceFactoryPool.weatherStationFactory.makeAppliance();
        WeatherStation weatherStation3 = (WeatherStation)  applianceFactoryPool.weatherStationFactory.makeAppliance();

        room11.addAppliance(airConditioner);
        room11.addAppliance(weatherStation1);
        room12.addAppliance(dishwasher);
        room12.addAppliance(thermostat);
        room11.addAppliance(weatherStation2);
        room13.addAppliance(dryer);
        room13.addAppliance(washingMachine);
        room13.addAppliance(humiditySensor);
        room11.addAppliance(weatherStation3);


        dryer.registerSensor(humiditySensor);
        dishwasher.registerSensor(humiditySensor);
        washingMachine.registerSensor(humiditySensor);

        this.addSportsEquipmentToRoom(room11);
        this.addSportsEquipmentToRoom(room12);
        this.addSportsEquipmentToRoom(room13);


        room11.addHouseholdMember(father);
        room12.addHouseholdMember(mother);
        room13.addHouseholdMember(child);

        Heating heating1 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();
        Heating heating2 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();
        Heating heating3 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();

        airConditioner.registerHeating(heating1);
        airConditioner.registerHeating(heating2);
        airConditioner.registerHeating(heating3);

        thermostat.registerHeating(heating1);
        thermostat.registerHeating(heating2);
        thermostat.registerHeating(heating3);


        room11.addAppliance(heating1);
        room12.addAppliance(heating2);
        room13.addAppliance(heating3);

        dishwasher.registerSensor(humiditySensor);
        dryer.registerSensor(humiditySensor);
        washingMachine.registerSensor(humiditySensor);

        return this;

    }

    @Override
    protected HouseFactory buildSecondFloor() {
        Floor floor1 = new Floor();
        house.getFloors().add(floor1);
        List<Room> rooms1 = new ArrayList<>();
        Room room11 = Generator.generateRoom();
        Room room12 = Generator.generateRoom();
        Room room13 = Generator.generateRoom();
        rooms1.add(room11);
        rooms1.add(room12);
        rooms1.add(room13);
        floor1.setRooms(rooms1);
        floor1.setLevel(1);

        AirConditioner airConditioner = (AirConditioner) applianceFactoryPool.airConditionerFactory.makeAppliance();
        Refrigirator refrigirator = (Refrigirator)  applianceFactoryPool.refrigiratorFactory.makeAppliance();
        Thermostat thermostat = (Thermostat) applianceFactoryPool.thermostatFactory.makeAppliance();
        WeatherStation weatherStation = (WeatherStation) applianceFactoryPool.weatherStationFactory.makeAppliance();
        WeatherStation weatherStation1 = (WeatherStation)  applianceFactoryPool.weatherStationFactory.makeAppliance();
        WeatherStation weatherStation2 = (WeatherStation)  applianceFactoryPool.weatherStationFactory.makeAppliance();
        WeatherStation weatherStation3 = (WeatherStation)  applianceFactoryPool.weatherStationFactory.makeAppliance();

        Heating heating1 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();
        Heating heating2 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();
        Heating heating3 = (Heating) applianceFactoryPool.heatingFactory.makeAppliance();

        thermostat.registerHeating(heating1);
        thermostat.registerHeating(heating2);
        thermostat.registerHeating(heating3);

        airConditioner.registerHeating(heating1);
        airConditioner.registerHeating(heating2);
        airConditioner.registerHeating(heating3);

        room11.addAppliance(refrigirator);
        room12.addAppliance(thermostat);
        room13.addAppliance(weatherStation);
        room13.addAppliance(heating1);
        room13.addAppliance(heating2);
        room13.addAppliance(heating3);
        room11.addAppliance(weatherStation1);
        room12.addAppliance(weatherStation2);
        room13.addAppliance(weatherStation3);

        this.addSportsEquipmentToRoom(room11);
        this.addSportsEquipmentToRoom(room12);
        this.addSportsEquipmentToRoom(room13);

        room11.addHouseholdMember(godfather);
        room12.addHouseholdMember(dog);
        room13.addHouseholdMember(cat);


        return this;
    }
}
