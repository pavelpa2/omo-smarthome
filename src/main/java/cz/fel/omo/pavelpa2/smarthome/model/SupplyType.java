package cz.fel.omo.pavelpa2.smarthome.model;

public enum SupplyType {
    WaterSupply, PowerSupply
}
