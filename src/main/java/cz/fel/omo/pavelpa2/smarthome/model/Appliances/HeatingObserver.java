package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

public interface HeatingObserver {
    void getNotified(String code);
}
