package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Thermostat;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public class ThermostatFactory extends ApplianceFactory{
    @Override
    public Appliance makeAppliance() {
        Thermostat dryer = new Thermostat();

        Resource electricity = this.elResourceFactory.makeResource(100);

        dryer.setApplianceType(ApplianceType.THERMOSTAT);
        dryer.addResource(electricity);

        dryer.setApplianceState(dryer.getStatePool().getOnState());

        return dryer;
    }
}
