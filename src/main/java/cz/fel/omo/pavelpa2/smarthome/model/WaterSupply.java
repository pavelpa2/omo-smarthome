package cz.fel.omo.pavelpa2.smarthome.model;

public class WaterSupply extends Supply {
    private static WaterSupply instance;
    private WaterSupply(){
        this.setPriceForUnit(0.2);
    }

    public static WaterSupply getInstance(){
        if (instance == null) {
            instance = new WaterSupply();
            instance.setType(SupplyType.WaterSupply);
        }
        return instance;
    }
}
