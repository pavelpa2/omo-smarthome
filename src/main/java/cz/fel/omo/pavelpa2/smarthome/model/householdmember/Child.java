package cz.fel.omo.pavelpa2.smarthome.model.householdmember;

import cz.fel.omo.pavelpa2.smarthome.util.Utilities;

public class Child extends Person{
    public Child(Education education, double finance, PersonRole role) {
        super(education, finance, role);
    }

    private void cry(){
        this.getReporter().writeEvent("Child#cry Child started crying", 0);
        nextChildCarerNode.handleChildCrying(this);
    }

    @Override
    public void interactWithSomeApplianceInRoom() {
        System.out.println("Child#InteractWithSomeApplianceInRoom");
        int randomInt = Utilities.getRandomNumber(0, 10);
        if (randomInt < 6){
            cry();
        }
    }
}
