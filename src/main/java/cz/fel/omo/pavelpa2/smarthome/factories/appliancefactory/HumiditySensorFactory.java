package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.HumiditySensor;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public class HumiditySensorFactory extends ApplianceFactory{
    @Override
    public Appliance makeAppliance() {
        HumiditySensor dryer = new HumiditySensor();

        Resource water = this.waterResourceFactory.makeResource(100);
        Resource electricity = this.elResourceFactory.makeResource(100);

        dryer.setApplianceType(ApplianceType.HUMIDITY_SENSOR);
        dryer.addResource(water);
        dryer.addResource(electricity);

        dryer.setApplianceState(dryer.getStatePool().getOnState());

        return dryer;
    }}
