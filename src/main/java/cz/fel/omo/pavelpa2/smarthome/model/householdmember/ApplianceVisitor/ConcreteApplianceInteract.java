package cz.fel.omo.pavelpa2.smarthome.model.householdmember.ApplianceVisitor;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.*;

public interface ConcreteApplianceInteract {
    boolean interact(AirConditioner airConditioner);
    boolean interact(Dishwasher dishwasher);
    boolean interact(Dryer dryer);
    boolean interact(HumiditySensor fireSensor);
    boolean interact(Heating heating);
    boolean interact(Refrigirator refrigirator);
    boolean interact(WashingMachine washingMachine);
    boolean interact(WeatherStation weatherStation);
    boolean interact(Thermostat thermostat);
}
