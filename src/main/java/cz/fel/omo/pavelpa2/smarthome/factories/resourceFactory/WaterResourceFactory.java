package cz.fel.omo.pavelpa2.smarthome.factories.resourceFactory;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.ResourceType;
import cz.fel.omo.pavelpa2.smarthome.model.WaterSupply;

public class WaterResourceFactory implements ResourceFactory{
    @Override
    public Resource makeResource(int onStateHourlyConsumption) {
        Resource waterResource = new Resource();
        waterResource.setResourceType(ResourceType.WATER);
        waterResource.setSupply(WaterSupply.getInstance());
        waterResource.setOnStateHourlyConsumption(onStateHourlyConsumption);
        waterResource.setConsumedTotal(0);
        return waterResource;
    }
}
