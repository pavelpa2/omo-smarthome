package cz.fel.omo.pavelpa2.smarthome;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.House;
import cz.fel.omo.pavelpa2.smarthome.model.Reporter;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

import java.io.IOException;

public class SmartHomeSimulation extends Simulation{
    private final Reporter reporter;
    private int day;
    private House house;

    public Reporter getReporter() {
        return reporter;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    public SmartHomeSimulation(House house, Reporter reporter) {
        this.house = house;
        this.reporter = reporter;
    }

    @Override
    void initialize() {
        this.day = 1;
        try {
            reporter.writeInputHouseConfig(this.house);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    void startSimulating() {
        System.out.println("cz.fel.omo.pavelpa2.smarthome.SmartHomeSimulation#startSimulating");
        while(day < 1000){
            reporter.writeEvent("**************************************Day "+day+"***********************",0);
            for (HouseholdMember h : house.getHouseholdMembers()){
                h.interactWithSomeApplianceInRoom();
            }
            for (Appliance a : house.getAllAppliances()){
                a.getApplianceState().handleConsumption(a);
            }
            day++;
            reporter.writeEvent("-------------------------------------evening-----------------------------", 0);
            for (HouseholdMember h : house.getHouseholdMembers()){
                h.comeBackHome();
            }
        }
    }

    @Override
    void endSimulating() {
        try {
            reporter.writeOutputHouseConfig(this.house);
            reporter.writeFinancialReport(this.house);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "cz.fel.omo.pavelpa2.smarthome.SmartHomeSimulation{" +
                "reporter=" + reporter +
                ", day=" + day +
                ", house=" + house +
                '}';
    }
}
