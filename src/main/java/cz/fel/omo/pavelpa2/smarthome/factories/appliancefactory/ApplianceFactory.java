package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.factories.resourceFactory.ElectricityResourceFactory;
import cz.fel.omo.pavelpa2.smarthome.factories.resourceFactory.ResourceFactory;
import cz.fel.omo.pavelpa2.smarthome.factories.resourceFactory.WaterResourceFactory;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;

public abstract class ApplianceFactory {
    ResourceFactory elResourceFactory = new ElectricityResourceFactory();
    ResourceFactory waterResourceFactory =  new WaterResourceFactory();

    public abstract Appliance makeAppliance();
}
