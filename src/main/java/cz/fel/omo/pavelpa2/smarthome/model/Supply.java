package cz.fel.omo.pavelpa2.smarthome.model;

abstract public class Supply {
    private int totalSupplied;
    private SupplyType type;
    private double priceForUnit;

    public void setPriceForUnit(double price) { this.priceForUnit = price;}

    public double getPriceForUnit() {
        return priceForUnit;
    }

    public int getTotalSupplied() {
        return totalSupplied;
    }

    public void setTotalSupplied(int totalSupplied) {
        this.totalSupplied = totalSupplied;
    }

    public SupplyType getType() {
        return type;
    }

    public void setType(SupplyType type) {
        this.type = type;
    }

    public double calculateTotalPrice(){
        return this.totalSupplied*priceForUnit;
    }
}
