package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import java.util.List;

public interface HeatingObservable {
    public void registerHeating(Heating h);
    public void registerHeating (List<Heating> h);
}
