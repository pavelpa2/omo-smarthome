package cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.*;

@JsonSerialize(as= ApplianceState.class)
public interface ApplianceState {
    public void turnOn(Appliance context);
    public void turnOff(Appliance context);
    public void breakAppliance(Appliance context);
    public void handleConsumption(Appliance context);
    public void handleChangeStateConsumption(Appliance context);
    public void consumeEveryResourceByFactor(Appliance context, int factor);
    public cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceState.Enum.ApplianceState getState();
}
