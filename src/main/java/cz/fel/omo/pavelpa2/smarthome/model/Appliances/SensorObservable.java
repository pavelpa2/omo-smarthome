package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

public interface SensorObservable {
    void registerSensor(Sensor sensor);
    void notifySensor();
}
