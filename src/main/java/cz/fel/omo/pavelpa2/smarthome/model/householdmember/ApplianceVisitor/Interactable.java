package cz.fel.omo.pavelpa2.smarthome.model.householdmember.ApplianceVisitor;

import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;

public interface Interactable {
    public boolean householdMemberInteracts(HouseholdMember h);
}
