package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

public interface SensorObserver {
    void getNotified();
}
