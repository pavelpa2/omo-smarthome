package cz.fel.omo.pavelpa2.smarthome.model.householdmember;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.*;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.fixappliancestrategy.FixApplianceStrategy;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.fixappliancestrategy.FixApplianceStrategyFixYourself;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.fixappliancestrategy.FixApplianceStrategyLazy;
import cz.fel.omo.pavelpa2.smarthome.util.Utilities;

public class Person extends HouseholdMember implements ChildCarerChainNode {
    @JsonIgnore
    protected Person nextChildCarerNode = null;
    private Education education;
    private double finance;
    private SportsEquipment sportsEquipment;
    @JsonIgnore
    private FixApplianceStrategy fixApplianceStrategy;

    public Person(Education education, double finance, PersonRole role) {
        this.education = education;
        this.finance = finance;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public double getFinance() {
        return finance;
    }

    public void setFinance(double finance) {
        this.finance = finance;
    }

    public SportsEquipment getSportsEquipment() {
        return sportsEquipment;
    }

    public void setSportsEquipment(SportsEquipment sportsEquipment) {
        this.sportsEquipment = sportsEquipment;
    }

    public FixApplianceStrategy getFixApplianceStrategy() {
        return fixApplianceStrategy;
    }

    public Person getNextChildCarerNode() {
        return nextChildCarerNode;
    }

    public void setFixApplianceStrategy(FixApplianceStrategy fixApplianceStrategy) {
        this.fixApplianceStrategy = fixApplianceStrategy;
    }

    private void takeFoodFromRefrigirator(Refrigirator refrigirator){
        refrigirator.setFoodCapacity(refrigirator.getFoodCapacity()-10);
    }

    private void loadFridge(Refrigirator refrigirator){
        refrigirator.setFoodCapacity(100);
        this.setFinance(this.getFinance() - 50);
    }

    private void feedChild(Child child){
        System.out.println("Person#feedChild");
        Room roomWithAFridge = this.getHouse().findRoomWithAFridge();
        if (roomWithAFridge == null){
            System.out.println("   No refrigirator in house");
            return;
        }
        this.visitRoom(roomWithAFridge);
        child.visitRoom(roomWithAFridge);

        Refrigirator refrigirator = roomWithAFridge.getRefrigiratorInRoom();

        this.takeFoodFromRefrigirator(refrigirator);
        child.setHunger(1);
    }

    private boolean fixAppliance(Appliance appliance){
        if (this.education == Education.HIGH_SCHOOL || this.education == Education.UNI) {
            this.fixApplianceStrategy = new FixApplianceStrategyFixYourself();
            this.getReporter().writeEvent("Person#fixAppliance fixing appliance", 2);
        }
        else {
            this.fixApplianceStrategy = new FixApplianceStrategyLazy();
            this.getReporter().writeEvent("Person#fixAppliance too lazy to fix appliance", 2);
        }
        return this.fixApplianceStrategy.fixAppliance(appliance);
    }

    private void eat(Refrigirator refrigirator){
        refrigirator.setFoodCapacity(refrigirator.getFoodCapacity() - 10);
        this.setHunger(this.getHunger() - 30);
    }

    @Override
    protected void goOutside() {
        SportsEquipment sportsEquipmentInRoom = this.getRoom().getAvailableSportsEquipment();
        if (sportsEquipmentInRoom != null) {
            this.setSportsEquipment(sportsEquipmentInRoom);
            getReporter().writeEvent("Person#goOutside picking up sports equipment " + sportsEquipmentInRoom, 2);
        }
        leaveRoom();
    }

    @Override
    public void comeBackHome() {
        if (this.isOutside()){
            getReporter().writeEvent("Person#comeBackHome "+this.getName()+" Came back home", 3);
            Room roomToComeBackTo = this.getHouse().getRandomRoomInHouse();
            this.setRoom(roomToComeBackTo);
            roomToComeBackTo.addHouseholdMember(this);
            if (this.getSportsEquipment() != null){
                getReporter().writeEvent("Person#comeBackHome "+this.getName()+" Returning sport equipment", 3);
                this.getRoom().addSportsEquipment(this.getSportsEquipment());
                this.setSportsEquipment(null);
            }
        }
    }

    @Override
    public void handleChildCrying(Child child){
        if (!this.isOutside()){
            this.getReporter().writeEvent("Person#handleChildCrying "+this.getName()+" feeding child", 1);
            feedChild(child);
            return;
        }
        if (nextChildCarerNode == null) {
            this.getReporter().writeEvent("Person#handleChildCrying "+this.getName()+" coming back home, feeding child", 1);
            this.comeBackHome();
            feedChild(child);
            return;
        }
        this.getReporter().writeEvent("Person#handleChildCrying "+this.getName()+" giving responsibilty to next node", 1);
        nextChildCarerNode.handleChildCrying(child);
        return;
    }

    @Override
    public void setNextChildCarerNode(Person p){
        if (this.nextChildCarerNode == null){
            this.nextChildCarerNode = p;
        }
    }

    public void deleteNextChildCarerNode(){
        this.nextChildCarerNode = null;
    }

    /***************Visitor for interacting*************/
    @Override
    public boolean interact(AirConditioner airConditioner) {
        if (airConditioner.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#airConditioner found broken appliance", 1);
            return fixAppliance(airConditioner);
        }
        this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#airConditioner changing target room temperature", 1);
        int randomRoomTemp = Utilities.getRandomNumber(18, 24);
        airConditioner.manuallySetTargetRoomTemperature(randomRoomTemp);
        return true;
    }

    @Override
    public boolean interact(Thermostat thermostat) {
        if (thermostat.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#thermostat found broken appliance", 1);
            return fixAppliance(thermostat);
        }
        this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#thermostat changing target room temperature", 1);
        int randomRoomTemp = Utilities.getRandomNumber(18, 24);
        thermostat.setTargetRoomTemperature(randomRoomTemp);
        return true;
    }

    @Override
    public boolean interact(Dishwasher dishwasher) {
        if (dishwasher.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#dishwasher found broken appliance", 1);
            return fixAppliance(dishwasher);
        }
        if (dishwasher.getDishesCapacity() > 80) {
            dishwasher.unloadDishwasher();
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#dishwasher unloaded dishwasher", 1);
        }
        else {
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#dishwasher inserting dish, turning dishwasher on and off", 1);
            dishwasher.insertDish();
            dishwasher.getApplianceState().turnOn(dishwasher);
            dishwasher.getApplianceState().turnOff(dishwasher);
        }
        return true;
    }

    @Override
    public boolean interact(Dryer dryer) {
        if (dryer.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#dryer found broken appliance", 1);
            return fixAppliance(dryer);
        }
        fixAppliance(dryer);
        dryer.getApplianceState().turnOn(dryer);
        dryer.getApplianceState().turnOff(dryer);
        this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#dryer turned on and off", 1);
        return true;
    }

    @Override
    public boolean interact(HumiditySensor fireSensor) {
        if (fireSensor.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#fireSensor found broken appliance", 1);
            return fixAppliance(fireSensor);
        }
        return false;
    }

    @Override
    public boolean interact(Heating heating) {
        if (heating.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#heating found broken appliance", 1);
            return fixAppliance(heating);
        }
        return false;
    }

    @Override
    public boolean interact(Refrigirator refrigirator) {
        if (refrigirator.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#refrigirator found broken appliance", 1);
            return fixAppliance(refrigirator);
        }
        if (refrigirator.getFoodCapacity() < 30) {
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#refrigirator loading fridge", 1);
            this.loadFridge(refrigirator);
            return true;
        }
        if (this.getHunger() > 70) {
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#refrigirator eating", 1);
            this.eat(refrigirator);
            return true;
        }
        return false;
    }

    @Override
    public boolean interact(WashingMachine washingMachine) {
        if (washingMachine.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#washingMachine found broken appliance", 1);
            return fixAppliance(washingMachine);
        }
        this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#washingMachine turning on and off", 1);
        washingMachine.getApplianceState().turnOn(washingMachine);
        washingMachine.getApplianceState().turnOff(washingMachine);
        return true;
    }

    @Override
    public boolean interact(WeatherStation weatherStation) {
        if (weatherStation.isBroken()){
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#weatherStation found broken appliance", 1);
            return fixAppliance(weatherStation);
        }
        int outsideWeather = weatherStation.getTodayForecast();
        if (outsideWeather > 15) {
            this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#WeatherStation looked at weather, going outside", 1);
            goOutside();
            return true;
        }
        this.getReporter().writeEvent(this.getClass().getSimpleName()+"#interact#WeatherStation looked at weather, staying home", 1);
        return false;
    }

    @Override
    public void interactWithSomeApplianceInRoom() {
        super.interactWithSomeApplianceInRoom();
    }

    @Override
    public String toString() {
        return this.getName();
    }
}
