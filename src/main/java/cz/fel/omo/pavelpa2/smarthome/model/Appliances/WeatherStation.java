package cz.fel.omo.pavelpa2.smarthome.model.Appliances;

import cz.fel.omo.pavelpa2.smarthome.model.Resource;
import cz.fel.omo.pavelpa2.smarthome.model.Room;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.HouseholdMember;
import cz.fel.omo.pavelpa2.smarthome.model.householdmember.Person;
import cz.fel.omo.pavelpa2.smarthome.util.Utilities;

import java.util.List;

public class WeatherStation extends Appliance {
    private int todayForecast;
    private List<Person> peopleInterestedInWeatherForecast;

    public WeatherStation() {

    }

    public WeatherStation(double price, String documentation, List<Resource> resources, Room room) {
        super(price, documentation, resources, room);
    }

    public int getTodayForecast(){
        this.todayForecast = Utilities.getRandomNumber(20, 25);
        return this.todayForecast;
    }

    @Override
    public boolean householdMemberInteracts(HouseholdMember h) {
       return h.interact(this);
    }

}
