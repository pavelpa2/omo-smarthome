package cz.fel.omo.pavelpa2.smarthome.factories.appliancefactory;

import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.ApplianceType;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Dishwasher;
import cz.fel.omo.pavelpa2.smarthome.model.Resource;

public class DishwasherFactory extends ApplianceFactory{
    @Override
    public Appliance makeAppliance() {
        Dishwasher dishwasher = new Dishwasher();
        dishwasher.setDishesCapacity(0);
        dishwasher.setDocumentation("Documentation");

        Resource water = this.waterResourceFactory.makeResource(100);
        Resource electricity = this.elResourceFactory.makeResource(100);

        dishwasher.setApplianceType(ApplianceType.DISHWASHER);
        dishwasher.addResource(water);
        dishwasher.addResource(electricity);

        dishwasher.setApplianceState(dishwasher.getStatePool().getOffState());

        return dishwasher;
    }
}
