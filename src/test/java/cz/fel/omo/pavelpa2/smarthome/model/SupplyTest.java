package cz.fel.omo.pavelpa2.smarthome.model;

import cz.fel.omo.pavelpa2.smarthome.Simulation;
import cz.fel.omo.pavelpa2.smarthome.SmartHomeSimulation;
import cz.fel.omo.pavelpa2.smarthome.factories.HouseFactory;
import cz.fel.omo.pavelpa2.smarthome.factories.PoorHouseFactory;
import cz.fel.omo.pavelpa2.smarthome.model.Appliances.Appliance;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class SupplyTest {
    House house;
    Reporter reporter = Reporter.getInstance();

    @BeforeEach
    public void setUp(){
        HouseFactory houseFactory = new PoorHouseFactory();

        try{
            house = houseFactory.buildHouse();

            Simulation smartHomeSimulation = new SmartHomeSimulation(house, reporter);
            smartHomeSimulation.simulate();

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Test
    public void totalConsumedEqualsSumOfApplianceUsage(){
        Optional<Supply> powerSupplyOpt= house.getSupplies().stream().filter(e->e.getType()== SupplyType.PowerSupply).findAny();
        Supply powerSupply = null;
        if (powerSupplyOpt.isPresent()){
            powerSupply = powerSupplyOpt.get();
        }
        assertNotNull(powerSupply);

        double totalConsumed = powerSupply.getTotalSupplied();

        double totalPowerConsumedAcrossAppliances = 0;
        for (Appliance a : house.getAllAppliances()){
            for (Resource r : a.getResources()){
                if (r.getResourceType() == ResourceType.ELECTRICITY){
                    totalPowerConsumedAcrossAppliances += r.getConsumedTotal();
                }
            }
        }
        assertEquals(totalConsumed, totalPowerConsumedAcrossAppliances);
    }
}