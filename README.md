<h3>Simulace domácnosti s chytrými spotřebiči.</h3>
V tomto projektu jsem si vyzkoušel aplikovat několik návrhových vzorů objektově orientovaného návrhu.
<br><br>
<h4>Funkce</h4>
- Každé zařízení se dá zapnout, vypnout, či rozbít na základě jejich stavu. <br>
- Lednice, susička, pračka a myčka mají své kapacity které se dají naplnit nebo spotřebovat. <br>
- Spotřebiče spotřebovávají (elektřinu, vodu) na základě jejich stavu (vypnutý, zapnutý, rozbitý) a při změně stavu. <br>
- Osoby a zvířata v domácnosti interagují se spotřebičem, jestliže jsou ve doma a ve stejné místnosti. Mohou jej např. zapnout nebo rozbít. Příklad využití Observer patternu je když člověk nastaví teplotu thermostatu na větší než je v pokoji, pak se zapnou topení. <br>
- Jestliže člověk nebo zvíře zjistí, že je venku hezky, může jít na zbytek dne ven, s sebou si vezme třeba nějaké sportovní náčiní, které později přinese zpátky domů. <br>
- Přiklad využití Chain of Responsibility patternu je, když se dítě rozbrečí. Tuto událost odbavuje postupně matka, otec... na základě toho, jestli jsou přitomní v domě. Pattern je využit i u snímače vlhkosti. Ten se spustí ve chvíli, když se rozbije spotřebič, který využívá vodu (pračka, myčka). <br>
- Dění v domě se simuluje na několik dní a na závěr jsou do textového souboru vygenerovány statistiky domu. <br>

<br>
<br>
<h4>Dále byly využity design patterny</h4>
State machine ... řeší stavy spotřebičů


Factory method ... Appliance factory a House factory


Singleton ... používam u WaterSpply a PowerSupply, protože při vytváření spotřebiče se hodí snadno získat instanci pro ,,Resource" daného spotřebiče. 

Visitor ... Využit u interagováni člena domácnosti (HouseholdMember) se spotřebičem (Appliance)
            
Template ... viz Simulation a SmartHomeSimulation

Object pool ... Appliance ma referenci na všechny své možné stavy (zapnut, vypnut, rozbit)

<h4>Entity diagram</h4>
![Entity diagram](entity_diagram_finish.drawio.png "Entity diagram")

<h4>Use case diagram</h4>
![Use case diagram](use_case_finish.drawio.png "Use case diagram")



